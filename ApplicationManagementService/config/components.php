<?php

Yii::$container->setSingleton('app\interfaces\IService', 'app\services\ApplicationService');
Yii::$container->setSingleton('app\interfaces\IRepository', 'app\repositories\ApplicationRepository');
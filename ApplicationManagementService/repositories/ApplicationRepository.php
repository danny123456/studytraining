<?php
namespace app\repositories;

use app\interfaces\IRepository;
use app\models\Application;
use yii\db\StaleObjectException;

class ApplicationRepository implements IRepository
{
    public function getList()
    {
        return Application::find()->asArray()->all();
    }

    public function update($data = [])
    {
        $product = new Application();
        if (isset($data['id'])) {
            $product = Application::findOne($data['id']);
        }

        foreach ($data as $name => $value) {
            $product->$name = $value;
        }

        try {
            $product->save();

            return $product;
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function delete($data = [])
    {
        if (isset($data['id'])) {
            $product = Application::findOne($data['id']);

            try {
                $product->delete();

                return $data;
            } catch (\Throwable $e) {
                return false;
            }
        }

        return false;
    }
}
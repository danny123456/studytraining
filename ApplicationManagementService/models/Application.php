<?php
namespace app\models;

use app\components\TaxBehavior;
use yii\db\ActiveRecord;

class Application extends ActiveRecord
{
    public function behaviors()
    {
        return [
            TaxBehavior::class
        ];
    }

    public static function tableName()
    {
        return "{{products}}";
    }
}
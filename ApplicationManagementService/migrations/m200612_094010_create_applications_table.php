<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%applications}}`.
 */
class m200612_094010_create_applications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%applications}}', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'study_plan_id' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk_application-studyPlan_id',
            'applications',
            'study_plan_id',
            'study_plans',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%applications}}');
    }
}

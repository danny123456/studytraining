<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%study_plans}}`.
 */
class m200612_094026_create_study_plans_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%study_plans}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(),
            'degree_id' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-studyPlan-course_id',
            'study_plans',
            'course_id',
            'courses',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_studyPlan-degree_id',
            'study_plans',
            'degree_id',
            'degrees',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%study_plans}}');
    }
}

<?php
namespace app\controllers;

use app\models\Application;
use sizeg\jwt\JwtHttpBearerAuth;
use yii\filters\Cors;

class ApplicationController extends BaseController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
//        $behaviors['authenticator'] = [
//            'class' => JwtHttpBearerAuth::class,
//            'optional' => [
//                'login',
//            ]
//        ];
//        $behaviors[] = [
//            'class' => Cors::class
//        ];

        return $behaviors;
    }

    public function actionList()
    {
        if (\Yii::$app->request->isGet) {
            $applications = $this->service->getList();
            return $this->_sendResponse(200, $applications, 'Get applications successfully');
        }

        return $this->_sendResponse(405, [], 'Method is not allowed!');
    }

    public function actionCreate()
    {
        $request = \Yii::$app->request;
        if ($request->isPost) {
            $appication = $this->service->update($request->post());

            if ($appication instanceof Application) {
                return $this->_sendResponse(200, $appication->toArray(), 'Product is created succesfully!');
            }

            return $this->_sendResponse(500, [], 'Cannot insert product');
        }

        return $this->_sendResponse(405, [], 'Method is not allowed!');
    }

    public function actionDelete()
    {
        $request = \Yii::$app->request;
        if ($request->isDelete) {
            $id = $this->service->delete($request->post());
            if ($id) {
                return $this->_sendResponse(200, $id, "Product is deleted successfully!" );
            }

            return $this->_sendResponse(500, [], 'Cannot delete product');
        }

        return $this->_sendResponse(405, [], 'Method is not allowed!');
    }
}
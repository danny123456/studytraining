<?php

Yii::$container->setSingleton('app\interfaces\IService', 'app\services\StudentService');
Yii::$container->setSingleton('app\interfaces\IRepository', 'app\repositories\StudentRepository');
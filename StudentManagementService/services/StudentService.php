<?php
namespace app\services;

use app\interfaces\IService;

class StudentService extends BaseService implements IService
{
    public function getList()
    {
        return $this->repository->getList();
    }

    public function update($data = [])
    {
        return $this->repository->update($data);
    }

    public function delete($data = [])
    {
        return $this->repository->delete($data);
    }
}
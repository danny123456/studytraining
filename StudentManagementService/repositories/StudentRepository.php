<?php
namespace app\repositories;

use app\interfaces\IRepository;
use app\models\Student;
use yii\db\StaleObjectException;

class StudentRepository implements IRepository
{
    public function getList()
    {
        return Student::find()->asArray()->all();
    }

    public function update($data = [])
    {
        $student = new Student();
        if (isset($data['id'])) {
            $student = Student::findOne($data['id']);
        }

        foreach ($data as $name => $value) {
            $student->$name = $value;
        }

        try {
            $student->save();

            return $student;
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function delete($data = [])
    {
        if (isset($data['id'])) {
            $student = Student::findOne($data['id']);

            try {
                $student->delete();

                return $data;
            } catch (\Throwable $e) {
                return false;
            }
        }

        return false;
    }
}
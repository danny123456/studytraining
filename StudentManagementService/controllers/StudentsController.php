<?php
namespace app\controllers;

use app\models\Student;
use sizeg\jwt\JwtHttpBearerAuth;
use yii\filters\Cors;

class StudentsController extends BaseController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        return $behaviors;
    }

    public function actionList()
    {
        if (\Yii::$app->request->isGet) {
            $students = $this->service->getList();
            return $this->_sendResponse(200, $students, 'Get students successfully');
        }

        return $this->_sendResponse(405, [], 'Method is not allowed!');
    }

    public function actionCreate()
    {
        $request = \Yii::$app->request;
        if ($request->isPost) {
            $student = $this->service->update($request->post());

            if ($student instanceof Student) {
                return $this->_sendResponse(200, $student->toArray(), 'Student is created succesfully!');
            }

            return $this->_sendResponse(500, [], 'Cannot insert student');
        }

        return $this->_sendResponse(405, [], 'Method is not allowed!');
    }

    public function actionDelete()
    {
        $request = \Yii::$app->request;
        if ($request->isDelete) {
            $id = $this->service->delete($request->post());
            if ($id) {
                return $this->_sendResponse(200, $id, "Student is deleted successfully!" );
            }

            return $this->_sendResponse(500, [], 'Cannot delete student');
        }

        return $this->_sendResponse(405, [], 'Method is not allowed!');
    }
}
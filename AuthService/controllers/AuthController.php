<?php
namespace app\controllers;

use app\interfaces\IAuthService;
use sizeg\jwt\JwtHttpBearerAuth;

class AuthController extends BaseController
{
    private $authService;

    public function __construct($id, $module, $config = [], IAuthService $authService)
    {
        $this->authService = $authService;
        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'login',
            ]
        ];

        return $behaviors;
    }

    public function actionLogin()
    {
        $request = \Yii::$app->request;
        if ($request->isPost) {
            $user = $this->authService->validate($request->post());
            if (!$user) {
                return $this->_sendResponse(404, [], 'User is not validated!');
            }

            $token = $this->authService->getToken($user);
            return $this->_sendResponse(200, ['token' => $token], 'User is logged in successfully!');
        }

        return $this->_sendResponse(405, [], 'Method is not allowed!');
    }
}
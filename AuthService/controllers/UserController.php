<?php
namespace app\controllers;

use app\interfaces\IUserService;
use app\models\Users;
use sizeg\jwt\JwtHttpBearerAuth;

class UserController extends BaseController
{
    private $userService;

    public function __construct($id, $module, $config = [], IUserService $userService)
    {
        $this->userService = $userService;
        parent::__construct($id, $module, $config);
    }

    public function actionCreate()
    {
        $request = \Yii::$app->request;

        if ($request->isPost) {
            $user = $this->userService->createUser($request->post());

            if ($user instanceof Users) {
                return $this->_sendResponse(200, $user->toArray(), 'User is created!');
            }

            if ($user instanceof \Exception) {
                return $this->_sendResponse(500, [], $user->getMessage());
            }

            return $this->_sendResponse(500, [], 'Something went wrong!');
        }

        return $this->_sendResponse(405, [], 'Method is not allowed!');
    }
}
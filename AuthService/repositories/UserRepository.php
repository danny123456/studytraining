<?php
namespace app\repositories;

use app\interfaces\IUserRepository;
use app\models\Users;

class UserRepository implements IUserRepository
{
    public function createUser($data = [])
    {
        $user = new Users();

        if (isset($data['id'])) {
            $user = Users::findOne($data['id']);
        }

        foreach ($data as $name => $value) {
            $user->$name = $value;
        }

        if (isset($data['password'])) {
            $user->password = \Yii::$app->getSecurity()->generatePasswordHash($data['password']);
        }

        $user->status = Users::STATUS_ACTIVE;

        try {
            $user->save();

            return $user;
        } catch (\Exception $e) {
            return $e;
        }
    }
}
<?php

Yii::$container->setSingleton('app\interfaces\IUserRepository', 'app\repositories\UserRepository');
Yii::$container->setSingleton('app\interfaces\IAuthService', 'app\services\AuthService');
Yii::$container->setSingleton('app\interfaces\IUserService', 'app\services\UserService');